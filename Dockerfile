# Use an official wordpressimage as the base image
FROM wordpress:latest

# Set the working directory inside the container
WORKDIR /var/www/html

# Create a volume mount point
VOLUME "$(pwd):/var/www/html"   

# Copy the content of the local "index.html" file to the container
COPY . .

# set environmental variables
ENV MYSQL_HOST=mysql   
ENV MYSQL_USER=root   
ENV MYSQL_PASSWORD=secret   
ENV MYSQL_DB=wordpress
